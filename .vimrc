set encoding=utf-8
set fileencoding=utf-8
set fileencodings=ucs-boms,utf-8,euc-jp,cp932
set fileformats=unix,dos,mac

set nobackup
set noswapfile
set showcmd

"hi SpecialKey ctermfg=242
"hi Search ctermfg=5
set background=dark

set number
set ruler
set cursorline

set showmatch
"set list listchars=tab:\$\>\>
set list listchars=tab:\|\>\>

set tabstop=3
set autoindent
set smartindent
set shiftwidth=3

set ignorecase
set smartcase
set incsearch
set wrapscan
set hlsearch
nmap <Esc><Esc> :nohlsearch<CR><Esc>

inoremap <silent> jj <ESC>

" 括弧を補完
inoremap { {}<Left>
inoremap {<Enter> {}<Left><CR><ESC><S-o>
inoremap {} {}
inoremap ( ()<ESC>i
inoremap (<Enter> ()<Left><CR><ESC><S-o>
inoremap () ()

inoremap " ""<ESC>i
inoremap "" ""

"Scripts-----------------------------
if &compatible
	set nocompatible " Be iMproved
endif

" Required:
set runtimepath+=$HOME/.vim/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('$HOME/.vim/dein')
	call dein#begin('$HOME/.vim/dein')

	call dein#add('$HOME/.vim/dein/repos/github.com/Shougo/dein.vim')
	" 追加したいプラグインを入れていく
	"以下２つは例
	"call dein#add('scrooloose/nerdtree')
	"call dein#add('raphamorim/lucario')
	call dein#add('cocopon/iceberg.vim')

	" Required:
	call dein#end()
	call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable
colorscheme iceberg

" If you want to install not installed plugins on startup.
if dein#check_install()
	call dein#install()
 endif

"End dein Scripts-------------------------



"NeoBundle
"Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if &compatible
  set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/

  " Required:
  call neobundle#begin(expand('~/.vim/bundle/'))

  " Let NeoBundle manage NeoBundle
  " Required:
  NeoBundleFetch 'Shougo/neobundle.vim'

  " My Bundles here:
  NeoBundle 'mattn/emmet-vim'
  " Refer to |:NeoBundle-examples|.
  " Note: You don't set neobundle setting in .gvimrc!

  call neobundle#end()

  " Required:
  filetype plugin indent on

  " If there are uninstalled bundles found on startup,
  " this will conveniently prompt you to install them.
  NeoBundleCheck
"NeoBundle
